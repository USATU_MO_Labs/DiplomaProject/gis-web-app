const merge = require("webpack-merge");
const common = require("./backend.common");

module.exports = merge(common, {
  mode: "production",
});
