const merge = require("webpack-merge");
const common = require("./frontend.common");

module.exports = merge(common, {
  mode: "development",
  devtool: "eval-source-map"
});
