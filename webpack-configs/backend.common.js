const path = require("path");
const CopyWebpackPlugin = require("copy-webpack-plugin");

module.exports = {
  target: "node",
  node: {
    __dirname: false,
    __filename: false
  },
  entry: path.resolve(__dirname, "../src/backend/backend-entry.ts"),
  output: {
    filename: "server.js",
    path: path.join(__dirname, "../dist")
  },
  // externals: [ "better-sqlite3" ],
  externals: { sqlite3: "commonjs sqlite3" },
  module: {
    rules: [
      {
        test: /\.ts$/,
        use: "ts-loader"
      }
    ]
  },
  resolve: {
    extensions: [".ts", ".js"]
  },
  plugins: [
    new CopyWebpackPlugin([
      { from: path.resolve(__dirname, "../database.db"), to: "database.db" }
    ])
  ]
};
