import $ from "jquery";
import noUiSlider from "nouislider";
import "nouislider/distribute/nouislider.css";
import "@chenfengyuan/datepicker/dist/datepicker.esm";
import "@chenfengyuan/datepicker/dist/datepicker.css";

import Cesium from "cesium/Cesium";
// import * as Cesium from "cesium"
import "cesium/Widgets/widgets.css";

import backend from "./backend-stub";
import { HeatmapImageryProvider } from "./HeatmapImageryProvider";

class InterfaceManager {
  constructor() {
    this.isReady = false;

    this._initCesium();
    this._initControls();

    // initial call
    this.isReady = true;
    this._onInputChange();
  }

  _initCesium() {
    Cesium.Ion.defaultAccessToken =
      "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJqdGkiOiI3NjM5ODAwYy1mOWRjLTQwNjktOGI1My0wOGZhY2FhNWQxNWUiLCJpZCI6MTE2MDksInNjb3BlcyI6WyJhc3IiLCJnYyJdLCJpYXQiOjE1NTkzODY5ODl9.eso19dHHVhtv2Ztd4wcy52K0vKnjJApmN4WdvwkuF2g";

    this.cesiumViewer = new Cesium.Viewer("cesiumContainer", {
      // навигатор по времени
      animation: false,
      // временная шкала
      timeline: false,
      // полный экран
      fullscreenButton: false,

      creditContainer: "cesiumCredits"
    });

    console.log("Initialized viewer");

    this.heatmapProvider = new HeatmapImageryProvider({
      bounds: {
        east: 180,
        west: -180,
        north: 90,
        south: -90
      }
    });
    const layers = this.cesiumViewer.scene.imageryLayers;
    this.heatmapLayer = layers.addImageryProvider(this.heatmapProvider);
  }

  _initControls() {
    // init datepicker
    this.datepickerId = "#datepicker";
    $(this.datepickerId).datepicker({
      autoPick: true,
      format: "dd/mm/yyyy",
      date: new Date(2018, 0, 1),
      startDate: new Date(2018, 0, 1),
      endDate: new Date(2018, 11, 31),
      weekStart: 1,
      pick: () => this._onInputChange()
    });

    // init slider
    // TODO show hour tooltip
    this.slider = noUiSlider.create(document.getElementById("hour-slider"), {
      range: {
        min: 0,
        max: 23
      },
      step: 1,
      start: 0
    });

    this.slider.on("update", () => this._onInputChange());

    // init radio buttons
    $("input#field-f[name=geomag-field-component]").prop("checked", true);
    $("input[type=radio][name=geomag-field-component]").change(e => {
      if ($(e.currentTarget).is(":checked")) {
        this._onInputChange();
      }
    });
  }

  async _onInputChange() {
    if (!this.isReady) return;

    // get date
    const date = $(this.datepickerId).datepicker("getDate");

    // get hour
    const hour = parseInt(this.slider.get(), 10);

    // get component
    const component = $(
      "input[type=radio][name=geomag-field-component]:checked"
    ).val();

    // fetch data and remake layer
    const grid = await backend.getGrid(date, hour, component);

    // poopy code, but whatever
    const mat_height = grid.length;
    const mat_width = grid[0].length;
    let min = Infinity;
    let max = -Infinity;
    const points = [];
    for (let i = 0; i < grid.length; i++) {
      for (let j = 0; j < grid[i].length; j++) {
        const v = grid[i][j];
        min = Math.min(min, v);
        max = Math.max(max, v);
        // x, y, value
        const point = {
          x: -180 + 360 * j / mat_width,
          y: -90 + 180 * i / mat_height,
          value: v
        }
        points.push(point);
      }
    }

    this.heatmapProvider.setWGS84Data(min, max, points);

    // redraw layer
    this.cesiumViewer.scene.imageryLayers.remove(this.heatmapLayer, false);
    this.cesiumViewer.scene.imageryLayers.add(this.heatmapLayer);
  }
}

export default new InterfaceManager();
