import request from "superagent";
import msgpack from "msgpack-lite";
import mem from "mem";

/**
 * @typedef {Object} Observatory
 * @property {string} iaga_code
 * @property {string} name
 * @property {string} country
 * @property {number} longitude
 * @property {number} latitude
 */

class BackendStub {
  constructor() {
    this.getObservatories = mem(this.getObservatories.bind(this));
  }

  /**
   *
   * @param {Date} date
   * @param {number} hour
   */
  async getObservatoryMeasurements(date, hour) {
    const { body: points } = await request.get("/api/observatory_measurements").query({
      year: date.getFullYear(),
      month: date.getMonth() + 1, // in JS months are 0-based
      day: date.getDate(),
      hour
    });

    return points;
  }

  /**
   *
   * @param {Date} date
   * @param {number} hour
   * @param {string} component x | y | z | f
   * @returns {number[][]}
   */
  async getGrid(date, hour, component) {
    const { body: bingrid } = await request.get("/api/grid").query({
      year: date.getFullYear(),
      month: date.getMonth() + 1, // in JS months are 0-based
      day: date.getDate(),
      hour,
      component
    }).responseType("arraybuffer");

    return msgpack.decode(new Uint8Array(bingrid));
  }

  /**
   *
   * @param {Date} date
   * @param {number} hour
   * @param {string} component x | y | z | f
   * @returns {number[][]}
   */
  async getGridVar(date, hour, component) {
    const { body: bingrid } = await request.get("/api/grid_var").query({
      year: date.getFullYear(),
      month: date.getMonth() + 1, // in JS months are 0-based
      day: date.getDate(),
      hour,
      component
    }).responseType("arraybuffer");

    return msgpack.decode(new Uint8Array(bingrid));
  }

  /**
   * @returns {Observatory[]}
   */
  async getObservatories() {
    const { body: observatories } = await request.get("/api/observatories");
    return observatories;
  }
}

export default new BackendStub();
