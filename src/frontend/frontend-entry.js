import "./index.css";

import mem from "mem";
import "./interface";
import backend from "./backend-stub";
import $ from "jquery";
import _ from "lodash";

async function init() {


  // TODO add mixin for clicks?

  /**
   * Callback for interface input changes
   * @param {Date} date
   * @param {number} hour
   */
  // let inputChangeCallback = async (date, hour) => {
  //   let points = await backend.getObservatoryMeasurements(date, hour);

  //   const heatmap_points = points.map(item => {
  //     const { iaga_code, x, y, z, f } = item;
  //     const obs = observatories[iaga_code];
  //     const { longitude, latitude } = obs;
  //     return {
  //       x: longitude,
  //       y: 90 - latitude, // colatitude to latitude
  //       value: f
  //     };
  //   });

  //   const vals = heatmap_points.map(p => p.value);
  //   const min = Math.min(...vals);
  //   const max = Math.max(...vals);

  //   heatmapProvider.setWGS84Data(min, max, heatmap_points);
  // };

  // inputChangeCallback = mem(inputChangeCallback);
  // const interfaceManager = new InterfaceManager(inputChangeCallback);
}

$(init);
