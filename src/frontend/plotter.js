import _ from "lodash";
import ndarray from "ndarray";
import interpolate from "ndarray-linear-interpolate";

/**
 * @typedef {Object} Point
 * @property {number} x
 * @property {number} y
 * @property {number} z
 */

/**
 * Disposable predictor with results caching.
 */
class Predictor {
  /**
   * @param {number} min
   * @param {number} max
   * @param {Point[]} data
   */
  constructor(min, max, data) {
    // normalize grid
    data = data.map(p => ({ x: p.x, y: p.y, z: (p.z - min) / (max - min) }));

    // interpolate
    // TODO get dimensions from data
    this.width = 72;
    this.height = 36;
    this.matrix = ndarray(data.map(p => p.z), [this.width, this.height]);
  }

  /**
   * Returns a value in [0, 1] range
   * @param {number} x [0, 1]
   * @param {number} y [0, 1]
   * @returns {number}
   */
  get(x, y) {
    return interpolate(this.matrix, x * this.width, y * this.height);
  }
}

export class GradientPlotter {
  /**
   * Plotter assumes that the canvas has fixed size.
   * @param {HTMLCanvasElement} canvas
   * @param {Array} colorMin [r, g, b]
   * @param {Array} colorMax [r, g, b]
   */
  constructor(canvas, colorMin, colorMax) {
    if (_.isUndefined(canvas)) {
      throw new Error("canvas is required");
    }

    if (_.isUndefined(colorMin)) {
      throw new Error("colorMin is required");
    }

    if (_.isUndefined(colorMax)) {
      throw new Error("colorMax is required");
    }

    this._canvas = canvas;
    this._colorMin = colorMin;
    this._colorMax = colorMax;

    this._colorDelta = [
      colorMax[0] - colorMin[0],
      colorMax[1] - colorMin[1],
      colorMax[2] - colorMin[2]
    ];
  }

  /**
   * Linear interpolation of color
   * @param {number} v value in range [0, 1]
   * @private
   */
  _lerpColor(v) {
    const color = [
      this._colorMin[0] + this._colorDelta[0] * v,
      this._colorMin[1] + this._colorDelta[1] * v,
      this._colorMin[2] + this._colorDelta[2] * v
    ];

    return color.map(Math.trunc);
  }

  /**
   * @param {Point[]} data
   */
  interpolateAndPlot(min, max, data) {
    console.log(min, max, data);
    const predictor = new Predictor(min, max, data);

    const ctx = this._canvas.getContext("2d");
    const width = this._canvas.width;
    const height = this._canvas.height;
    const imageData = ctx.getImageData(0, 0, width, height);
    const pixelData = imageData.data;

    /** coords to pixel index in imageData (if each pixel is 4 bytes) */
    const xytoi = (x, y) => y * imageData.width + x;

    // draw pixels
    for (let x = 0; x < imageData.width; x++) {
      for (let y = 0; y < imageData.height; y++) {
        // get normalized value
        let v = predictor.get(x / imageData.width, y / imageData.height);

        // lerp color
        const color = this._lerpColor(v);

        // pixel at (x, y), first byte index
        const i = xytoi(x, y) * 4;
        pixelData[i + 0] = color[0];
        pixelData[i + 1] = color[1];
        pixelData[i + 2] = color[2];
        pixelData[i + 3] = 255;
      }
    }

    ctx.putImageData(imageData, 0, 0);
  }

  /**
   * @param {Point[]} data
   */
  interpolateAndPlot_old(min, max, data) {
    // normalize data
    data = data.map(p => ({ x: p.x, y: p.y, z: (p.z - min) / (max - min) }));

    // prepare small image
    var smallCanvas = document.createElement("canvas");
    var sctx = smallCanvas.getContext("2d");
    // TODO properly set from data?
    smallCanvas.width = 72; // 360 degrees with a step of 5 degrees
    smallCanvas.height = 36; // 180 degrees with a step of 5 degrees
    const imageData = sctx.createImageData(smallCanvas.width, smallCanvas.height);
    const pixelData = imageData.data;

    // draw small image
    /** coords to pixel index in imageData (if each pixel is 4 bytes) */
    const xytoi = (x, y) => y * imageData.width + x;

    for (const p of data) {
      const x = Math.trunc((p.x + 180) / 360);
      const y = Math.trunc((p.y + 90) / 180);
      const v = p.z;

      // lerp color
      const color = this._lerpColor(v);

      // pixel at (x, y), first byte index
      const i = xytoi(x, y) * 4;
      pixelData[i + 0] = color[0];
      pixelData[i + 1] = color[1];
      pixelData[i + 2] = color[2];
      pixelData[i + 3] = 255;
    }

    sctx.putImageData(imageData, 0, 0);

    // draw small image to main canvas with automatic scaling
    const ctx = this._canvas.getContext("2d");
    const width = this._canvas.width;
    const height = this._canvas.height;
    ctx.drawImage(smallCanvas, 0, 0, width, height);
  }
}
