import Cesium from "cesium/Cesium";
// import * as Cesium from "cesium";
import _ from "lodash";
// import h337 from "heatmapjs/heatmap";
import { GradientPlotter } from "./plotter";

export class HeatmapImageryProvider /*extends Cesium.ImageryProvider*/ {
  /**
   * Provides a single, top-level imagery tile.  The single image is assumed to use a
   * {@link GeographicTilingScheme}.

    *
    * @alias HeatmapImageryProvider
    * @constructor
    *
    * @param {Object} options Object with the following properties:
    * @param {Object} options.bounds The bounding box for the heatmap in WGS84 coordinates.
    * @param {number} options.bounds.north The northernmost point of the heatmap.
    * @param {number} options.bounds.south The southernmost point of the heatmap.
    * @param {number} options.bounds.west The westernmost point of the heatmap.
    * @param {number} options.bounds.east The easternmost point of the heatmap.
    * @param {Object} options.data Data to be used for the heatmap.
    * @param {Object} options.data.min Minimum allowed point value.
    * @param {Object} options.data.max Maximum allowed point value.
    * @param {Array} options.data.points The data points for the heatmap containing x=lon, y=lat and value=number.
    *
    * @see HeatmapImageryProvider
    * @see ArcGisMapServerImageryProvider
    * @see BingMapsImageryProvider
    * @see GoogleEarthImageryProvider
    * @see OpenStreetMapImageryProvider
    * @see TileMapServiceImageryProvider
    * @see WebMapServiceImageryProvider
    */
  constructor(options) {
    this.credit = null;
    this.defaultAlpha = 1.0;
    this.defaultBrightness = 1.0;
    this.defaultContrast = 1.0;
    this.defaultGamma = 1.0;
    this.defaultHue = 0.0;
    this.defaultSaturation = 1.0;
    this.defaultMagnificationFilter = undefined;
    this.defaultMinificationFilter = undefined;

    options = _.defaultTo(options, {});
    let bounds = options.bounds;

    if (_.isUndefined(bounds)) {
      throw new Cesium.DeveloperError("options.bounds is required.");
    } else if (
      _.isUndefined(bounds.north) ||
      _.isUndefined(bounds.south) ||
      _.isUndefined(bounds.east) ||
      _.isUndefined(bounds.west)
    ) {
      throw new Cesium.DeveloperError(
        "options.bounds.north, options.bounds.south, options.bounds.east and options.bounds.west are required."
      );
    }

    this._wmp = new Cesium.WebMercatorProjection();
    this._mbounds = this.wgs84ToMercatorBB(bounds);
    // this._options = _.defaultTo(options.heatmapoptions, {});
    // this._options.gradient = _.defaultTo(this._options.gradient, {
    //   0.25: "rgb(0,0,255)",
    //   0.55: "rgb(0,255,0)",
    //   0.85: "yellow",
    //   1.0: "rgb(255,0,0)"
    // });

    this._setWidthAndHeight(this._mbounds);
    // this._options.radius = Math.round(
    //   _.defaultTo(
    //     this._options.radius,
    //     this.width > this.height ? this.width / 60 : this.height / 60
    //   )
    // );

    this._xoffset = this._mbounds.west;
    this._yoffset = this._mbounds.south;

    // this._spacing = this._options.radius * 1.5;

    // this.width = Math.round(this.width + this._spacing * 2);
    // this.height = Math.round(this.height + this._spacing * 2);

    // this._mbounds.west -= this._spacing * this._factor;
    // this._mbounds.east += this._spacing * this._factor;
    // this._mbounds.south -= this._spacing * this._factor;
    // this._mbounds.north += this._spacing * this._factor;

    this.bounds = this.mercatorToWgs84BB(this._mbounds);

    this._canvas = this._getCanvas(
      this.width,
      this.height,
      "imagery-provider-plotter-canvas"
    );
    // this._options.container = this._container;
    // this._heatmap = h337.create(this._options);
    this._plotter = new GradientPlotter(
      this._canvas,
      [0, 0, 255],
      [255, 0, 0]
    );

    this._tilingScheme = new Cesium.WebMercatorTilingScheme({
      rectangleSouthwestInMeters: new Cesium.Cartesian2(
        this._mbounds.west,
        this._mbounds.south
      ),
      rectangleNortheastInMeters: new Cesium.Cartesian2(
        this._mbounds.east,
        this._mbounds.north
      )
    });

    this._image = this._canvas;
    this._texture = undefined;
    this._tileWidth = this.width;
    this._tileHeight = this.height;
    // this._ready = false;
    this._ready = true;

    if (options.data) {
      this.setWGS84Data(
        options.data.min,
        options.data.max,
        options.data.points
      );
    }
  }

  /**
   * Gets the URL of the single, top-level imagery tile.
   * @type {string}
   * @readonly
   */
  get url() {
    return this._url;
  }

  /**
   * Gets the width of each tile, in pixels. This function should
   * not be called before {@link HeatmapImageryProvider#ready} returns true.
   * @type {number}
   * @readonly
   */
  get tileWidth() {
    if (!this._ready) {
      throw new Cesium.DeveloperError(
        "tileWidth must not be called before the imagery provider is ready."
      );
    }

    return this._tileWidth;
  }

  /**
   * Gets the height of each tile, in pixels.  This function should
   * not be called before {@link HeatmapImageryProvider#ready} returns true.
   * @type {number}
   * @readonly
   */
  get tileHeight() {
    if (!this._ready) {
      throw new Cesium.DeveloperError(
        "tileHeight must not be called before the imagery provider is ready."
      );
    }

    return this._tileHeight;
  }

  /**
   * Gets the maximum level-of-detail that can be requested.  This function should
   * not be called before {@link HeatmapImageryProvider#ready} returns true.
   * @type {number}
   * @readonly
   */
  get maximumLevel() {
    if (!this._ready) {
      throw new Cesium.DeveloperError(
        "maximumLevel must not be called before the imagery provider is ready."
      );
    }

    return 0;
  }

  /**
   * Gets the minimum level-of-detail that can be requested.  This function should
   * not be called before {@link HeatmapImageryProvider#ready} returns true.
   * @type {number}
   * @readonly
   */
  get minimumLevel() {
    if (!this._ready) {
      throw new Cesium.DeveloperError(
        "minimumLevel must not be called before the imagery provider is ready."
      );
    }

    return 0;
  }

  /**
   * Gets the tiling scheme used by this provider.  This function should
   * not be called before {@link HeatmapImageryProvider#ready} returns true.
   * @type {TilingScheme}
   * @readonly
   */
  get tilingScheme() {
    if (!this._ready) {
      throw new Cesium.DeveloperError(
        "tilingScheme must not be called before the imagery provider is ready."
      );
    }

    return this._tilingScheme;
  }

  /**
   * Gets the rectangle, in radians, of the imagery provided by this instance.  This function should
   * not be called before {@link HeatmapImageryProvider#ready} returns true.
   * @type {Rectangle}
   * @readonly
   */
  get rectangle() {
    if (!this._ready) {
      throw new Cesium.DeveloperError(
        "rectangle must not be called before the imagery provider is ready."
      );
    }

    return this._tilingScheme.rectangle; //TODO: change to custom rectangle?
  }

  /**
   * Gets the tile discard policy.  If not undefined, the discard policy is responsible
   * for filtering out "missing" tiles via its shouldDiscardImage function.  If this function
   * returns undefined, no tiles are filtered.  This function should
   * not be called before {@link HeatmapImageryProvider#ready} returns true.
   * @type {TileDiscardPolicy}
   * @readonly
   */
  get tileDiscardPolicy() {
    if (!this._ready) {
      throw new Cesium.DeveloperError(
        "tileDiscardPolicy must not be called before the imagery provider is ready."
      );
    }

    return undefined;
  }

  /**
   * Gets an event that is raised when the imagery provider encounters an asynchronous error.  By subscribing
   * to the event, you will be notified of the error and can potentially recover from it.  Event listeners
   * are passed an instance of {@link TileProviderError}.
   * @type {Event}
   * @readonly
   */
  get errorEvent() {
    return this._errorEvent;
  }

  /**
   * Gets a value indicating whether or not the provider is ready for use.
   * @type {Boolean}
   * @readonly
   */
  get ready() {
    return this._ready;
  }

  /**
   * Gets the credit to display when this imagery provider is active.  Typically this is used to credit
   * the source of the imagery.  This function should not be called before {@link HeatmapImageryProvider#ready} returns true.
   * @type {Credit}
   * @readonly
   */
  // get credit() {
  //   return this._credit;
  // }

  /**
   * Gets a value indicating whether or not the images provided by this imagery provider
   * include an alpha channel.  If this property is false, an alpha channel, if present, will
   * be ignored.  If this property is true, any images without an alpha channel will be treated
   * as if their alpha is 1.0 everywhere.  When this property is false, memory usage
   * and texture upload time are reduced.
   * @type {Boolean}
   * @readonly
   */
  get hasAlphaChannel() {
    return true;
  }

  _setWidthAndHeight(mbb) {
    let maxCanvasSize = 2000;
    let minCanvasSize = 700;
    this.width =
      mbb.east > 0 && mbb.west < 0
        ? mbb.east + Math.abs(mbb.west)
        : Math.abs(mbb.east - mbb.west);
    this.height =
      mbb.north > 0 && mbb.south < 0
        ? mbb.north + Math.abs(mbb.south)
        : Math.abs(mbb.north - mbb.south);
    this._factor = 1;

    if (this.width > this.height && this.width > maxCanvasSize) {
      this._factor = this.width / maxCanvasSize;

      if (this.height / this._factor < minCanvasSize) {
        this._factor = this.height / minCanvasSize;
      }
    } else if (this.height > this.width && this.height > maxCanvasSize) {
      this._factor = this.height / maxCanvasSize;

      if (this.width / this._factor < minCanvasSize) {
        this._factor = this.width / minCanvasSize;
      }
    } else if (this.width < this.height && this.width < minCanvasSize) {
      this._factor = this.width / minCanvasSize;

      if (this.height / this._factor > maxCanvasSize) {
        this._factor = this.height / maxCanvasSize;
      }
    } else if (this.height < this.width && this.height < minCanvasSize) {
      this._factor = this.height / minCanvasSize;

      if (this.width / this._factor > maxCanvasSize) {
        this._factor = this.width / maxCanvasSize;
      }
    }

    this.width = this.width / this._factor;
    this.height = this.height / this._factor;
  }

  _getCanvas(width, height, id) {
    let c = document.getElementById(id);

    if (c) {
      return c;
    }

    c = document.createElement("canvas");
    c.setAttribute("id", id);
    c.setAttribute("style", "margin: 0px; display: none;");
    c.setAttribute("width", width);
    c.setAttribute("height", height);
    document.body.appendChild(c);

    return c;
  }

  /**
   * Convert a WGS84 location into a Mercator location.
   *
   * @param {Object} point The WGS84 location.
   * @param {number} [point.x] The longitude of the location.
   * @param {number} [point.y] The latitude of the location.
   * @returns {Cesium.Cartesian3} The Mercator location.
   */
  wgs84ToMercator(point) {
    return this._wmp.project(Cesium.Cartographic.fromDegrees(point.x, point.y));
  }

  /**
   * Convert a WGS84 bounding box into a Mercator bounding box.
   *
   * @param {Object} bounds The WGS84 bounding box.
   * @param {number} [bounds.north] The northernmost position.
   * @param {number} [bounds.south] The southrnmost position.
   * @param {number} [bounds.east] The easternmost position.
   * @param {number} [bounds.west] The westernmost position.
   * @returns {Object} The Mercator bounding box containing north, south, east and west properties.
   */
  wgs84ToMercatorBB(bounds) {
    let ne = this._wmp.project(
      Cesium.Cartographic.fromDegrees(bounds.east, bounds.north)
    );
    let sw = this._wmp.project(
      Cesium.Cartographic.fromDegrees(bounds.west, bounds.south)
    );
    return {
      north: ne.y,
      south: sw.y,
      east: ne.x,
      west: sw.x
    };
  }

  /**
   * Convert a mercator location into a WGS84 location.
   *
   * @param {Object} point The Mercator lcation.
   * @param {number} [point.x] The x of the location.
   * @param {number} [point.y] The y of the location.
   * @returns {Object} The WGS84 location.
   */
  mercatorToWgs84(p) {
    let wp = this._wmp.unproject(new Cesium.Cartesian3(p.x, p.y));
    return {
      x: wp.longitude,
      y: wp.latitude
    };
  }

  /**
   * Convert a Mercator bounding box into a WGS84 bounding box.
   *
   * @param {Object} bounds The Mercator bounding box.
   * @param {number} [bounds.north] The northernmost position.
   * @param {number} [bounds.south] The southrnmost position.
   * @param {number} [bounds.east] The easternmost position.
   * @param {number} [bounds.west] The westernmost position.
   * @returns {Object} The WGS84 bounding box containing north, south, east and west properties.
   */
  mercatorToWgs84BB(bounds) {
    let sw = this._wmp.unproject(
      new Cesium.Cartesian3(bounds.west, bounds.south)
    );
    let ne = this._wmp.unproject(
      new Cesium.Cartesian3(bounds.east, bounds.north)
    );
    return {
      north: this.rad2deg(ne.latitude),
      east: this.rad2deg(ne.longitude),
      south: this.rad2deg(sw.latitude),
      west: this.rad2deg(sw.longitude)
    };
  }

  /**
   * Convert degrees into radians.
   *
   * @param {number} degrees The degrees to be converted to radians.
   * @returns {number} The converted radians.
   */
  deg2rad(degrees) {
    return degrees * (Math.PI / 180.0);
  }

  /**
   * Convert radians into degrees.
   *
   * @param {number} radians The radians to be converted to degrees.
   * @returns {number} The converted degrees.
   */
  rad2deg(radians) {
    return radians / (Math.PI / 180.0);
  }

  /**
   * Convert a WGS84 location to the corresponding heatmap location.
   *
   * @param {Object} point The WGS84 location.
   * @param {number} [point.x] The longitude of the location.
   * @param {number} [point.y] The latitude of the location.
   * @returns {Object} The corresponding heatmap location.
   */
  wgs84PointToHeatmapPoint(point) {
    return this.mercatorPointToHeatmapPoint(this.wgs84ToMercator(point));
  }

  /**
   * Convert a mercator location to the corresponding heatmap location.
   *
   * @param {Object} point The Mercator lcation.
   * @param {number} [point.x] The x of the location.
   * @param {number} [point.y] The y of the location.
   * @returns {Object} The corresponding heatmap location.
   */
  mercatorPointToHeatmapPoint(point) {
    let pn = {};

    pn.x = Math.round((point.x - this._xoffset) / this._factor /*+ this._spacing*/);
    pn.y = Math.round((point.y - this._yoffset) / this._factor /*+ this._spacing*/);
    pn.y = this.height - pn.y;

    return pn;
  }

  /**
   * Set an array of heatmap locations.
   *
   * @param {number} min The minimum allowed value for the data points.
   * @param {number} max The maximum allowed value for the data points.
   * @param {Array} data An array of data points with heatmap coordinates(x, y) and value
   * @returns {Boolean} Wheter or not the data was successfully added or failed.
   */
  setData(min, max, data) {
    if (
      data &&
      data.length > 0 &&
      min !== null &&
      min !== false &&
      max !== null &&
      max !== false
    ) {
      this._plotter.interpolateAndPlot(
        min,
        max,
        data.map(d => ({ x: d.x, y: d.y, z: d.value }))
      );

      return true;
    }

    return false;
  }

  /**
   * Set an array of WGS84 locations.
   *
   * @param {number} min The minimum allowed value for the data points.
   * @param {number} max The maximum allowed value for the data points.
   * @param {Array} data An array of data points with WGS84 coordinates(x=lon, y=lat) and value
   * @returns {Boolean} Wheter or not the data was successfully added or failed.
   */
  setWGS84Data(min, max, data) {
    if (
      data &&
      data.length > 0 &&
      min !== null &&
      min !== false &&
      max !== null &&
      max !== false
    ) {
      let convdata = [];

      for (let i = 0; i < data.length; i++) {
        let gp = data[i];

        let hp = this.wgs84PointToHeatmapPoint(gp);
        if (gp.value || gp.value === 0) {
          hp.value = gp.value;
        }

        convdata.push(hp);
      }

      return this.setData(min, max, convdata);
    }

    return false;
  }

  /**
   * Gets the credits to be displayed when a given tile is displayed.
   *
   * @param {number} x The tile X coordinate.
   * @param {number} y The tile Y coordinate.
   * @param {number} level The tile level;
   * @returns {Credit[]} The credits to be displayed when the tile is displayed.
   *
   * @exception {Cesium.DeveloperError} <code>getTileCredits</code> must not be called before the imagery provider is ready.
   */
  getTileCredits(x, y, level) {
    return undefined;
  }

  /**
   * Requests the image for a given tile.  This function should
   * not be called before {@link HeatmapImageryProvider#ready} returns true.
   *
   * @param {number} x The tile X coordinate.
   * @param {number} y The tile Y coordinate.
   * @param {number} level The tile level.
   * @returns {Promise} A promise for the image that will resolve when the image is available, or
   *          undefined if there are too many active requests to the server, and the request
   *          should be retried later.  The resolved image may be either an
   *          Image or a Canvas DOM object.
   *
   * @exception {Cesium.DeveloperError} <code>requestImage</code> must not be called before the imagery provider is ready.
   */
  requestImage(x, y, level) {
    if (!this._ready) {
      throw new Cesium.DeveloperError(
        "requestImage must not be called before the imagery provider is ready."
      );
    }

    console.log("Requesting image from HeatmapImageryProvider");

    return this._image;
  }

  /**
   * Picking features is not currently supported by this imagery provider, so this function simply returns
   * undefined.
   *
   * @param {number} x The tile X coordinate.
   * @param {number} y The tile Y coordinate.
   * @param {number} level The tile level.
   * @param {number} longitude The longitude at which to pick features.
   * @param {number} latitude  The latitude at which to pick features.
   * @return {Promise} A promise for the picked features that will resolve when the asynchronous
   *                   picking completes.  The resolved value is an array of {@link ImageryLayerFeatureInfo}
   *                   instances.  The array may be empty if no features are found at the given location.
   *                   It may also be undefined if picking is not supported.
   */
  pickFeatures() {
    return undefined;
  }
}
