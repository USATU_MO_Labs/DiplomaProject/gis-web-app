import express from "express";
import path from "path";
import restApiRouter from "./api/rest-api";

const app = express();
const port = process.env.PORT || 8080;

console.log(__dirname);
app.use("/", express.static(path.join(__dirname, "frontend")));
app.use("/api", restApiRouter);

app.listen(port, () => console.log(`App listening on port ${port}!`));
