import sqlite3 from "sqlite3";

sqlite3.verbose();

export interface ITimePoint {
  year: number;
  month: number;
  day: number;
  hour: number;
}

export interface IObservatoryMeasurement {
  year: number;
  month: number;
  day: number;
  hour: number;
  iaga_code: string;
  x: number;
  y: number;
  z: number;
  f: number;
}

export interface IObservatory {
  iaga_code: string;
  name: string;
  country: string;
  latitude: number;
  longitude: number;
}

export interface IObservatories {
  [iagaCode: string]: IObservatory;
}

export interface IGrid {
  year: number;
  month: number;
  day: number;
  hour: number;
  component: string;
  grid_msgpack: Buffer;
  grid_var_msgpack: Buffer;
}

export class DatabaseService {
  private db: sqlite3.Database;
  private getObservatoryMeasurementsStatement: sqlite3.Statement;
  private getObservatoriesStatement: sqlite3.Statement;
  private getGridStatement: sqlite3.Statement;

  constructor() {
    // после сборки бд будет лежать рядом с бандлом
    const dbPath = "database.db";
    this.db = new sqlite3.Database(dbPath, sqlite3.OPEN_READONLY);

    this.getObservatoryMeasurementsStatement = this.db.prepare(
      `
      select
        year,
        month,
        day,
        hour,
        iaga_code,
        x, y, z, f
      from
        time_series
      where
        year=? and
        month=? and
        day=? and
        hour=?
      `,
      (_: any, err: any) => err && console.error(err),
    );

    this.getObservatoriesStatement = this.db.prepare(
      `
      select
        iaga_code,
        name,
        country,
        latitude,
        longitude
      from
        observatories
      `,
      (_: any, err: any) => err && console.error(err),
    );

    this.getGridStatement = this.db.prepare(
      `
      select
        grid_msgpack,
        grid_var_msgpack
      from
        grid
      where
        year=? and
        month=? and
        day=? and
        hour=? and
        component=?
      `,
      (_: any, err: any) => err && console.error(err),
    );
  }

  public getObservatoryMeasurements(
    time: ITimePoint,
  ): Promise<IObservatoryMeasurement[]> {
    const { year, month, day, hour } = time;
    return new Promise((resolve, reject) => {
      this.getObservatoryMeasurementsStatement.all(
        [year, month, day, hour],
        (err, rows) => (err ? reject(err) : resolve(rows)),
      );
    });
  }

  public getTimeRange(): Promise<{ min: ITimePoint; max: ITimePoint }> {
    return new Promise((resolve, reject) => {
      // TODO
    });
  }

  public getGrid(time: ITimePoint, component: string): Promise<IGrid> {
    const { year, month, day, hour } = time;
    return new Promise((resolve, reject) => {
      if (!["x", "y", "z", "f"].includes(component)) {
        reject(`Invalid component ${component}`);
      }

      this.getGridStatement.get([year, month, day, hour, component], (err, row) => {
        // console.log(row);
        return err ? reject(err) : resolve(row);
      }
      );
    });
  }

  public getObservatories(): Promise<IObservatories> {
    return new Promise((resolve, reject) => {
      this.getObservatoriesStatement.all((err, rows: IObservatory[]) => {
        if (err) {
          reject(err);
        }

        const result: IObservatories = {};

        rows.forEach((row) => (result[row.iaga_code.toLowerCase()] = row));

        resolve(result);
      });
    });
  }
}
