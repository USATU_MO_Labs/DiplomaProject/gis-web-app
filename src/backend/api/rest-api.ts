import express from "express";
import _ from "lodash";
import {
  generateRandomPoints,
  IRandomPointsConstraintsOptional,
} from "./random-points";
import { DatabaseService } from "../database/database-service";

const router = express.Router();
const dbService = new DatabaseService();

router.get("/random_points", (req, res) => {
  const count: number = req.query.count;
  const options: IRandomPointsConstraintsOptional = {
    longtitude: {
      min: req.query.minLongtitude,
      max: req.query.maxLongtitude,
    },
    latitude: {
      min: req.query.minLatitude,
      max: req.query.maxLatitude,
    },
    height: {
      min: req.query.minHeight,
      max: req.query.maxHeight,
    },
  };

  const points = generateRandomPoints(count, options);

  res.json(points);
});

router.get("/observatory_measurements", async (req, res) => {
  console.log("/observatory_measurements", req.query);
  const { year, month, day, hour } = req.query;
  if (!_.every([year, month, day, hour], _.negate(_.isUndefined))) {
    res.status(400).send("Bad request");
    return;
  }

  const measurements = await dbService.getObservatoryMeasurements({
    year,
    month,
    day,
    hour,
  });
  res.json(measurements);
});

router.get("/grid", async (req, res) => {
  console.log("/grid", req.query);
  const { year, month, day, hour, component } = req.query;
  if (!_.every([year, month, day, hour, component], _.negate(_.isUndefined))) {
    res.status(400).send("Bad request");
    return;
  }

  try {
    const grid = await dbService.getGrid({ year, month, day, hour }, component);
    const data = grid.grid_msgpack;
    res.send(data);
  } catch (e) {
    res.status(400).send(e);
  }
});

router.get("/grid_var", async (req, res) => {
  console.log("/grid_var", req.query);
  const { year, month, day, hour, component } = req.query;
  if (!_.every([year, month, day, hour, component], _.negate(_.isUndefined))) {
    res.status(400).send("Bad request");
    return;
  }

  try {
    const grid = await dbService.getGrid({ year, month, day, hour }, component);
    const data = grid.grid_var_msgpack;
    res.send(data);
  } catch (e) {
    res.status(400).send(e);
  }
});

router.get("/timerange", async (req, res) => {
  // TODO
  res.status(501).send("Not implemented");
});

router.get("/observatories", async (req, res) => {
  const observatories = await dbService.getObservatories();
  res.json(observatories);
});

export default router;
