import _ from "lodash";

export interface IRandomPointsConstraintsOptional {
  longtitude?: {
    min?: number;
    max?: number;
  };
  latitude?: {
    min?: number;
    max?: number;
  };
  height?: {
    min?: number;
    max?: number;
  };
}

interface IRandomPointsConstraints {
  longtitude: {
    min: number;
    max: number;
  };
  latitude: {
    min: number;
    max: number;
  };
  height: {
    min: number;
    max: number;
  };
}

export interface IRandomPoint {
  longtitude: number;
  latitude: number;
  height: number;
}

export function generateRandomPoints(count: number, options: IRandomPointsConstraintsOptional): IRandomPoint[] {
  const constraints: IRandomPointsConstraints = _.defaultsDeep(options, {
    longtitude: {
      min: -180,
      max: 180,
    },
    latitude: {
      min: -180,
      max: 180,
    },
    height: {
      min: 0,
      max: 10000,
    },
  });

  const points: IRandomPoint[] = [];

  for (let i = 0; i < count; i++) {
    const longtitude = _.random(constraints.longtitude.min, constraints.longtitude.max, true);
    const latitude = _.random(constraints.latitude.min, constraints.longtitude.max, true);
    const height = _.random(constraints.height.min, constraints.height.max, true);

    points.push({
      longtitude,
      latitude,
      height,
    });
  }

  return points;
}
